package StepDefinitions;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DemoLogin {
	
	static WebDriver driver = null;
		@Given("user is on Home Page of {string}")
		public void user_is_on_home_page_of(String string) {
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
			driver.get("http://demowebshop.tricentis.com/");
			
			System.out.println("Launch the brower");
		}
		@When("user click on login button")
		public void user_click_on_login_button() {
			
			System.out.println("User able to login button");
		}
		@When("user should be navigate to login page")
		public void user_should_be_navigate_to_login_page() {
			driver.findElement(By.xpath("//a[.='Log in']")).click();
			System.out.println("user able to give credentials");
		}
		@When("user enter email {string} and password {string}")
		public void user_enter_email_and_password(String string, String string2) {
			driver.findElement(By.id("Email")).sendKeys("saikiranbodala@gmail.com");
			driver.findElement(By.id("Password")).sendKeys("123456");
		
			System.out.println("user give here to credentials");
		}
		@Then("user should get logged in")
		public void user_should_get_logged_in() {
			driver.findElement(By.xpath("//input[@value='Log in']")).click();
			System.out.println("user successfully logedin");
		}

}
