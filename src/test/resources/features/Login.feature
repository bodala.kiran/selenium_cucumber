Feature: Demo Web Shop validation
Description: This feature will test a Login functionality
Scenario: Verify Login functionality
Given user is on Home Page of "https://demowebshop.tricentis.com/"
When user click on login button
And user should be navigate to login page
And user enter email "saikiranbodala@gmail.com" and password "123456"
And user click on login button
Then user should get logged in
